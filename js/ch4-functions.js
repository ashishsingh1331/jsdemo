/**
 * Created by ashish on 6/3/17.
 */

//Anonymous function(function without name)
// but these Function can be stored as data
var a = function (a) {
    return a;
}


//Callback Functions

function one(){
    return 1;
}

function two(){
    return 2;
}

function invoke_and_add(a,b){
    return a() + b();
}

 console.log(invoke_and_add(one,two));


//self invoking function
(
function(){
    alert('hii')
})();

(
    function(name){
        alert('Hello ' + name + '!');
    }
)('dude')


//inner private function , functions can be used internally
function a(param) {
    function b(theinput) {
        return theinput * 2;
    };
    return 'The result is ' + b(param);
};

//function that return function

function a() {
    alert('A!');
    return function(){
        alert('B!');
    };
}

//Scope of a variable

var global = 1;
function f() {
    var local = 2;
    global++;
    return global;
}

console.log(global);
console.log(local);


