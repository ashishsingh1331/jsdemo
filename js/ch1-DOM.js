/**
 * Created by ashish on 6/3/17.
 */
var btn = document.getElementById('button');
var appendbtn = document.getElementById('append');
var changeColor = document.getElementById('changeColor');

console.log(console.dir(document));

btn.addEventListener('click', function () {
    var p1Text = document.getElementById('first').innerHTML;
    var p2Text = document.getElementById('second').innerHTML;


    document.getElementById('first').innerHTML = p2Text;
    document.getElementById('second').innerHTML = p1Text;

})
appendbtn.addEventListener('click', function () {
   var pTag =  document.createElement('p');
    var textNode = document.createTextNode('Appened p tag');
    pTag.appendChild(textNode);
    document.getElementById('pContainer').appendChild(pTag);
})
changeColor.addEventListener('click', function () {
    var pTags = document.getElementsByTagName('p');

    for(var i=0;i< pTags.length;i++){
        pTags[i].style.color = '#'+(Math.random()*0xFFFFFF<<0).toString(16);
    }

})
