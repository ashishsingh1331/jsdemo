/**
 * Created by ashish on 23/3/17.
 */
// functions 'remember' the environment in which they were created.
 alert('Closures functions \'remember\' the environment in which they were created.');
function makeFunc() {
    var name = 'Mozilla';
    function displayName() {
        alert(name);
    }
    return displayName;
}
var myFunc = makeFunc();
//myFunc();

var counter = function() {
    var privateCounter = 0;
    function changeBy(val) {
        privateCounter += val;
    }
    return {
        increment: function() {
            changeBy(1);
        },
        decrement: function() {
            changeBy(-1);
        },
        value: function() {
            return privateCounter;
        }
    };
};
var counter1 = counter();
var counter2 = counter();

console.log(counter1.increment());
console.log('counter 1 value ', counter1.value())
console.log('counter 2 value ', counter2.value())