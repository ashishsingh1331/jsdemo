/**
 * Created by ashish on 6/3/17.
 */

function Product() {
}
Product.prototype = {
    name: 'defaultName',
    price: 'defaultPrice',
    inStock: 'defaultInStock',
    category: 'defaultCategory',
    details: function () {
        console.log(this.name + this.category)
    }
}
Product.prototype.constructor = Product;


//Books
function Book(name, price, inStock, author, language) {
    this.name = name;
    this.price = price;
    this.inStock = inStock;
    this.category = 'Book'
    this.author = author;
    this.language = language;
    this.getBookAuthor = function () {
        return this.author;
    }
}
Book.prototype = Product.prototype;
Book.prototype.constructor = Book;

//Mobiles
function Mobiles(name, price, inStock, category, brand) {
    this.name = name;
    this.price = price;
    this.inStock = inStock;
    this.category = 'Mobile';
    this.brand = brand;
}

Mobiles.prototype = Product.prototype;
Mobiles.prototype.constructor = Mobiles;


function BasicPhone(name, price, inStock, category, brand,keyPad) {
    this.name = name;
    this.price = price;
    this.inStock = inStock;
    this.category = 'Mobile';
    this.brand = brand;
    this.keypad = keyPad;
}
BasicPhone.prototype = Mobiles.prototype;
BasicPhone.constructor = BasicPhone;


//book
var lastLecture = new Book('Last lecture', 160, 'Randy Pausch', 'English');
console.log(lastLecture);

var motoG = new Mobiles('Moto G', 7000, true, 'Mobiles', 'Motorola');

var nokia1100 = new BasicPhone('Moto G', 7000, true, 'Mobiles', 'Nokia', true);
console.log(motoG);
console.log(motoG.details());
console.log(lastLecture.details());
